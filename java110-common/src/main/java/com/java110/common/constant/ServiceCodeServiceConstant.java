package com.java110.common.constant;

/**
 * 服务常量类
 * Created by wuxw on 2017/5/20.
 */
public class ServiceCodeServiceConstant {

    /**
     * 添加 服务
     */
    public static final String ADD_SERVICE = "service.saveService";


    /**
     * 修改 服务
     */
    public static final String UPDATE_SERVICE = "service.updateService";
    /**
     * 删除 服务
     */
    public static final String DELETE_SERVICE = "service.deleteService";


    /**
     * 查询 服务
     */
    public static final String LIST_SERVICES = "service.listServices";



    /**
     * 绑定 服务
     */
    public static final String BINDING_SERVICE = "service.bindingService";


}
